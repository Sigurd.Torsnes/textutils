package no.uib.ii.inf112;


import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;


public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			if (width > text.length()) {
				int spaces = Math.abs(text.length() - width);
				return " ".repeat(spaces) + text;
			}
			return text;
		}

		public String flushLeft(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String justify(String text, int width) {
			
			
			String result = "";
			String[] words = text.split(" ");
			ArrayList<Integer> spaces = new ArrayList<>();
			int numOfPaddings = words.length-1;
			
			int numLetters = 0;
			for (String word : words) {
				numLetters += word.length();
			}
			int spacesToAdd = width-numLetters;
			
			for (int i=0; i<numOfPaddings; i++) {
				spaces.add(spacesToAdd/numOfPaddings);
				
			}
			
			for (int i = 0; i<spacesToAdd%numOfPaddings; i++) {
				spaces.set(i,spaces.get(i) + 1);
			}
			
			for (int i=0; i<spaces.size()+1; i++) {
				result += words[i];
				if (i<spaces.size())
					result += " ".repeat(spaces.get(i));
			}
			return result;
		}};
		
	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
	}
	
	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
	}
	
	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("AAA", aligner.flushRight("AAA",3));
		assertEquals("AAA", aligner.flushRight("AAA",2));
		assertEquals("AAA", aligner.flushRight("AAA",-3));
		assertEquals("AAA ", aligner.flushRight("AAA ",2));
	}
	
	@Test
	void testJustify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
		assertEquals("fee    fie   foo", aligner.justify("fee fie foo", 16));
		assertEquals("fee    fie   foo", aligner.justify("fee   fie foo", 16));
		assertEquals("fee   fie   foo", aligner.justify("fee fie   foo", 15));
		assertEquals("fee    fie", aligner.justify("fee fie", 10));
	}
	
	@Test
	void testJustifyInput() {
	    Exception exception = assertThrows(Exception.class, () -> {
	        Integer.parseInt("1a");
	    });

	    String expectedMessage = "";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	
}
